using System.Collections.Generic;

namespace org.example {


    class GenericPair<K,V> : GenericPairInterface<K,V>
    {
        private K key;
        private V val;

        public GenericPair(K key, V val)
        {
            this.key = key;
            this.val = val;
        }


        public K getKey(){ return key; }
        public V getValue() { return val; }

        public void setKey(K key) { this.key = key; }
        public void setValue(V val) { this.val = val; }

    }

}

