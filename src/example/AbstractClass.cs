namespace org.example {

    abstract class AbstractClass : ChildInterface
    {
        public abstract void abstractMethod();
        public abstract void childInterfaceMethod();
        public abstract void baseInterfaceMethod();
    }

}
