namespace org.example {

    interface GenericPairInterface<K,V>
    {
        K getKey();
        V getValue();
    }

}
