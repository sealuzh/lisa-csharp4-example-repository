namespace org.example {

    public class CustomException : System.ApplicationException
    {
        public CustomException() {}
        public CustomException(string message) {}
        public CustomException(string message, System.Exception inner) {}

        protected CustomException(System.Runtime.Serialization.SerializationInfo info,
                System.Runtime.Serialization.StreamingContext context) {}
    }

}
