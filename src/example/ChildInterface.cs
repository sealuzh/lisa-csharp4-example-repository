namespace org.example {

    interface ChildInterface : BaseInterface
    {
        void childInterfaceMethod();
    }

}
