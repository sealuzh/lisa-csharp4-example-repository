namespace org.example {

    class ConcreteSubclass : ConcreteClass
    {
        override public int returnsSomethingMethod() { return 101; }
    }

}
