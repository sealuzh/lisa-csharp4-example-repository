namespace org.example {
    public class BrainMethod : CodeSmell {
        private void brainMethod() {
            // To qualify as a "Brain Method", it needs to exhibit these metrics:
            // more than 65 lines, or 6*65 = 390 vertices
            // more than 0.24 mcc per loc or 0.24/6 = 0.04 mcc per vertex or 15.60 mcc
            // more than 2 MAXNESTING
            // more than 8 variable access

            // need 16 cyclo and 2 nesting, so we use 2x3 if statements (= 4*4 MCC)
            if (true) { if (true) { if (true) { }}}
            if (true) { if (true) { if (true) { }}}

            // need 390 (~32*12) vertices and 9 variables in total
            // we already have 60 vertices from the if conditions above, so we need
            // another 330 vertices. Each of the 15 lines below has 22 vertices.
            int n1 = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            int n2 = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            int n3 = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            int n4 = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            int n5 = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            int n6 = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            int n7 = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            int n8 = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            int nn = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            nn = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            nn = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            nn = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            nn = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            nn = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;
            nn = 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11;

        }
    }
}
