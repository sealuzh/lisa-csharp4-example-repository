namespace org.example {

    // UniquePaths: 354
    public class ControlFlow {

        // Members0 - UniquePaths: 2, MCC: 2
        public void singleIf() {
            if (true) {
            }
        }

        // Members1 - UniquePaths: 3, MCC: 3
        public void twoNestedIfs() {
            if (true) {
                if (true) {
                }
            }
        }

        // Members2 - UniquePaths: 4, MCC: 3
        public void twoConsecutiveIfs() {
            if (true) {
            }
            if (true) {
            }
        }

        // Members3 - UniquePaths: 10, MCC: 5
        public void simpleIfTree() {
            // UniquePaths: 5
            if (true) {
                if (true) {
                }
                if (true) {
                }
            }
            // UniquePaths: 2
            if (true) {
            }
        }

        // Members4 - UniquePaths: 30, MCC: 7
        public void complexIfTree() {
            // UniquePaths: 3
            if (true) {
                if (true) {
                }
            }
            // UniquePaths: 2
            if (true) {
            }
            // UniquePaths: 5
            if (true) {
                if (true) {
                }
                if (true) {
                }
            }
        }

        // Members5 - UniquePaths: 2, MCC: 2
        public bool orOperator() {
            return false || true;
        }

        // Members6 - UniquePaths: 3, MCC: 3
        public void orOperatorInsideIf() {
            if (false || true) {
            }
        }

        // Members7 - UniquePaths: 4, MCC: 4
        public void twoOrOperatorsInsideIf() {
            if (false || false || true) {
            }
        }

        // Members8 - UniquePaths: 2, MCC: 2
        public bool ternaryOperator() {
            return false ? false : true;
        }

        // Members9 - UniquePaths: 3, MCC: 3
        public void ternaryOperatorInsideIf() {
            if (false ? false : true) {
            }
        }

        // Members10 - UniquePaths: 4, MCC: 4
        public void switchThreeCases() {
            switch (1) {
                case 1: break;
                case 2: break;
                case 3: break;
            }
        }

        // Members11 - UniquePaths: 10, MCC: 10
        public void nestedSwitchThreeTwoOneCases() {
            switch (1) {
                case 1:
                    // UniquePaths: 4, MCC: 4
                    switch (1) {
                        case 1: break;
                        case 2: break;
                        case 3: break;
                    }
                    break;
                case 2:
                    // UniquePaths: 3, MCC: 3
                    switch (1) {
                        case 1: break;
                        case 2: break;
                    }
                    break;
                case 3:
                    // UniquePaths: 2, MCC: 2
                    switch (1) {
                        case 1: break;
                    }
                    break;
            }
        }

        // Members12 - UniquePaths: 12, MCC: 6
        public void consecutiveSwitchThreeTwoCases() {
            // UniquePaths: 4, MCC: 4
            switch (1) {
                case 1: break;
                case 2: break;
                case 3: break;
            }
            // UniquePaths: 3, MCC: 3
            switch (1) {
                case 1: break;
                case 2: break;
            }
        }

        // Members13 - UniquePaths: 12, MCC: 6
        public void consecutiveSwitchThreeTwoCasesNoBreaks() {
            // UniquePaths: 4, MCC: 4
            switch (1) {
                case 1: ;
                case 2: ;
                case 3: ;
            }
            // UniquePaths: 3, MCC: 3
            switch (1) {
                case 1: ;
                case 2: ;
            }
        }

        // Members14 - UniquePaths: 2, MCC: 2
        public void singleFor() {
            for(int i=1; i<10; i++) { }
        }

        // Members15 - UniquePaths: 3, MCC: 3
        public void twoNestedFors() {
            for(int i=1; i<10; i++) {
                for(int j=1; j<10; j++) { }
            }
        }

        // Members16 - UniquePaths: 4, MCC: 3
        public void twoConsecutiveFors() {
            for(int i=1; i<10; i++) { }
            for(int i=1; i<10; i++) { }
        }

        // Members17 - UniquePaths: 2, MCC: 2
        public void singleForWithContinue() {
            for(int i=1; i<10; i++) { continue; }
        }

        // Members18 - UniquePaths: 2, MCC: 2
        public void singleWhile() {
            bool b = false;
            while (b) { }
        }

        // Members19 - UniquePaths: 3, MCC: 3
        public void twoNestedWhiles() {
            bool b = false;
            while (b) {
                while (b) { }
            }
        }

        // Members20 - UniquePaths: 4, MCC: 3
        public void consecutiveWhiles() {
            bool b = false;
            while (b) { }
            while (b) { }
        }

        // Members21 - UniquePaths: 2, MCC: 2
        public void singleWhileWithBreak() {
            bool b = false;
            while (b) { break; }
        }

        // Members22 - UniquePaths: 2, MCC: 2
        public void singleWhileWithContinue() {
            bool b = false;
            while (b) { continue; }
        }

        // Members23 - UniquePaths: 8, MCC: 8
        public void switchWithIfWhilesInCasesWithBreaks() {
            bool b = false;
            switch (1) {
                case 1: if (false) { while (b) { continue; } }
                            break;
                case 2:
                        break;
                case 3: if (true) { if (true) { } }
                            break;
            }
        }

        // Members24 - UniquePaths: 16 (because of fallthroughs), MCC: 8
        public void switchWithIfWhilesInCasesWithoutBreaks() {
            bool b = false;
            // UniquePaths: 9 + 3 + 3 + 1
            switch (1) {
                // cases 1-3 -> UniquePaths 9
                case 1: if (false) { while (b) { continue; } }
                            // cases 2-3 -> UniquePaths 3
                case 2:
                            // case 3 -> UniquePaths 3
                case 3: if (true) { if (true) { } }
            }
        }

        // Members25 - UniquePaths: 252, MCC: 16
        public void complexMixedExample() {
            bool b = false;
            // UniquePaths: 4
            if (true) {
                if (true) {
                    while (b) { }
                }
            }
            // UniquePaths: 9
            if (true) {
                // UniquePaths: 8
                switch (1) {
                    case 1:
                        // UniquePaths: 4
                        switch (1) {
                            case 1: break;
                            case 2: break;
                            case 3: break;
                        }
                        break;
                    case 2:
                        // UniquePaths: 3
                        switch (1) {
                            case 1: break;
                            case 2: break;
                        }
                        break;
                }
            }
            // UniquePaths: 7
            if (true) {
                // UniquePaths: 3
                if (true) {
                    for(int j=1; j<10; j++) { }
                }
                // UniquePaths: 2
                if (true) {
                }
            }

        }

    }

}
