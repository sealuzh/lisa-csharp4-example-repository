namespace org.example {

    class ConcreteClass : AbstractClass
    {

        private int instanceVariable;

        private int privateField = 1;
        protected int protectedField = 1;
        public int publicField = 1;

        private static int privateStaticField = 1;
        protected static int protectedStaticField = 1;
        public static int publicStaticField = 1;

        override public void abstractMethod() { }
        override public void childInterfaceMethod() { }
        override public void baseInterfaceMethod() { }


        private void privateMethod() { }

        protected void protectedMethod() { }

        public void mccMethod() {
            if (true) {
                if (true) {
                    if (true) {
                    }
                }
            }
        }

        public void accessesFieldsMethod() {
            privateField++;
            protectedField++;
            publicField++;
        }

        public void hasParametersMethod(string s, char c, int i, double d, float f,
                long l, short sh, bool b) { }

        public virtual int returnsSomethingMethod() { return 1; }

        public void invokesMethodMethod() {
            returnsSomethingMethod();
        }

        public void throwsExceptionMethod () {
            throw new CustomException("error");
        }

        public int throwsExceptionMethod (int param) {
            throw new CustomException("error");
            return 1;
        }

        public ConcreteClass() {
            instanceVariable = 1;
        }

        public ConcreteClass(int constructorParam) {
            instanceVariable = constructorParam;
        }

        public class InnerClass {

            public int f1() { return 1; }

            public class InnerInnerClass {
                public int f2() { return 2; }
                public int f3() { if (true) { return 3; } return 4; }
            }

        }

        public void addedMethod1() { }



    }

}
