using System;
using System.Diagnostics;

namespace org.example {

    public class Hello
    {
        public static void Main(string[] args)
        {


            GenericPair<int, string> pair = new GenericPair<int, string>(1, "hello");
            GenericPair<int, string> qualifiedPair = new org.example.GenericPair<int, string>(1, "hello");
            int key = pair.getKey();
            string value = pair.getValue();
            Debug.Assert(key == 1);
            Debug.Assert(value == "hello");

            ConcreteClass instance = new ConcreteClass();
            ConcreteClass instanceConstructedWithParams = new ConcreteClass(1);
            org.example_sibling.SiblingClass instanceSibling = new org.example_sibling.SiblingClass();
            org.example.subpackage.SubpackageClass instanceSubpackage =
              new org.example.subpackage.SubpackageClass();
            org.example.subpackage.subsubpackage.SubsubpackageClass instanceSubsubpackage =
              new org.example.subpackage.subsubpackage.SubsubpackageClass();
            ConcreteClass.InnerClass instanceInnerClass = new ConcreteClass.InnerClass();
            ConcreteClass.InnerClass.InnerInnerClass instanceInnerInnerClass = new ConcreteClass.InnerClass.InnerInnerClass();
            ConcreteClass nullVar = null;

            ControlFlow cf = new ControlFlow();

            CodeSmell brainMethod = new BrainMethod();

            instance.invokesMethodMethod(); // need to resolve to actual method in class

            double one = Math.Cos(Math.PI * 2);
            float two = 2f;


            int x = 10;
            while (x > 0) {
                x--;
                --x;
            }
            Debug.Assert(x <= 0);

            bool assertionThing = false;
            if (1 > 2) {
            }
            else {
                if (200 > 100) {
                    if (2000 > 1000) {
                        assertionThing = true;
                    }
                }
            }
            Debug.Assert(assertionThing == true);

            Debug.Assert(instance.returnsSomethingMethod() == 1);
            instance = new ConcreteSubclass();
            Debug.Assert(instance.returnsSomethingMethod() == 101);

            System.Console.WriteLine("Main function finished");


        }
    }

}
