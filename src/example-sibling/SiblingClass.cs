namespace org.example_sibling {

    public class SiblingClass {
        public int publicSiblingVariable = 1;
        public SiblingClass() {
            int privateSiblingVariable = 1;
        }

    }
}
